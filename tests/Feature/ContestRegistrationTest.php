<?php

namespace Tests\Feature;

use App\Events\NewEntryReceivedEvent;
use App\Mail\WelcomeContestMail;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Mail;
use Tests\TestCase;

class ContestRegistrationTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void {
        parent::setUp();
        
          Mail::fake();
    }
    
    //** @test */
    public function test_an_email_can_be_entered_into_the_contest()
    {
        $this->post('/contest', [
            'email' => 'abc@abc.com'
        ]);
        
        $this->assertDatabaseCount('contest_entries', 1);
    }
    
    //** @test */
    public function test_email_is_required() 
    {
        $this->post('/contest', [
            'email' => ''
        ]);
        
        $this->assertDatabaseCount('contest_entries', 0);
    }
    
    /*** @test*/
    public function test_an_event_is_fired_when_user_registers()
    {  
        Event::fake([
            NewEntryReceivedEvent::class
        ]);
        
        $this->post('/contest', [
            'email' => 'abc@abc.com'
        ]);
        
        Event::assertDispatched(NewEntryReceivedEvent::class);
    }
    
    /**@test*/
    public function test_a_welcome_email_is_sent()
    {   
        $this->post('/contest', [
            'email' => 'abc@abc.com',
        ]);
        
        Mail::assertQueued(WelcomeContestMail::class);
    }
}
